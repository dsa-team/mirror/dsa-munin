#!/bin/bash

# Copyright 2007, 2011 Peter Palfrader <peter@palfrader.org>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#%# family=auto
#%# capabilities=autoconf suggest

findconfig() {
    local conf
    local thisver thiscluster thisport
    conf=${0##*postgres-wal-traffic_}
    if [ -z "$conf" ]; then
        echo >&2 "Do not know for which cluster to get stats"
        exit 1
    fi

    mycluster=$(
        pg_lsclusters --no-header | \
        while read thisver thiscluster thisport dummy; do
            if [ "${thisver}_${thiscluster}" = "$conf" ] ; then
                echo $thisport $thisver $thiscluster
                break
            fi
        done
    )

    if [ -z "$mycluster" ]; then
        echo >&2 "Did not find port for $conf"
        exit 1
    fi

    read port ver cluster <<< "$mycluster"
}

if [ "$1" = "autoconf" ]; then
    if which pg_lsclusters > /dev/null 2>&1 ; then
        echo yes
    else
        echo "no (pg_lsclusters not available)"
    fi
elif [ "$1" = "suggest" ]; then
    pg_lsclusters --no-header | while read ver cluster dummy; do
        echo "${ver}_${cluster}"
    done
elif [ "$1" = "config" ]; then
    findconfig
    echo "graph_title Postgres WAL Traffic for $cluster (pg $ver on port $port)"
    echo 'graph_args -l 0'
    echo 'graph_vlabel bytes/min'
    echo 'graph_info This graph shows amount of transaction log traffic'
    echo 'walt.label WAL Traffic'
    echo 'walt.cdef walt,60,*'
    echo 'walt.type DERIVE'
    echo 'walt.draw AREA'
else
    findconfig
    info=$(psql -p "$port" --no-align --command 'SELECT * FROM pg_current_xlog_insert_location()'  --tuples-only --quiet | tr -d /)
    info="0x$info"
    bytes="$(( $info ))"
    echo "walt.value $bytes"
fi
# vim:set et:
# vim:set ts=4:
# vim:set shiftwidth=4:
